---
title: Install Nvidia Drivers on Garuda Sway
description: 
published: true
date: 2024-02-01T02:59:16.980Z
tags: 
editor: markdown
dateCreated: 2023-12-27T18:04:36.358Z
---

The Sway project officially [does not support Nvidia drivers](https://github.com/swaywm/sway/wiki#nvidia-users). This has always been the case, and they have always been very up front about this.

From the project page linked above:

> ### Nvidia users
>
> All proprietary graphics drivers are not officially supported, including the Nvidia proprietary driver. The open source Nouveau driver can be used instead. Do not ask questions regarding the Nvidia proprietary driver here. If you have a choice of hardware, keep open source support in mind! 


The Nouveau drivers work great, especially for older cards, and do not need any additional configuration. The Nouveau project has come a long way, and for many common use cases these drivers will do just fine.

If you do configure Sway to run with the proprietary drivers, it would be reasonable to expect them to break from time to time--especially when the Nvidia drivers update from one version to the next. These two projects are all but incompatible, and functionality is explicitly not guaranteed. It may be helpful to familiarize yourself with the process of downgrading the Nvidia drivers for troubleshooting.

With all that out of the way: you *can* run Sway with the proprietary Nvidia drivers with a little additional configuration.

> **Do not install the proprietary drivers during the installation process** (when you are installing Garuda). When booting to the live ISO, **choose the open source driver option**. Complete the installation, bring your system fully up to date, and reboot before continuing with the driver installation.{.is-warning}

{ }

## Install the Drivers

First, install the drivers with whatever package is appropriate for your specific GPU and kernel combination. In this example I am using `nvidia-dkms`, but of course you should refer to the ArchWiki article and figure out what package you should be using. https://wiki.archlinux.org/title/NVIDIA#Installation

`nvidia-utils` should be pulled in as a dependency of the driver package and does not need to be explicitly installed.

Other packages installed during testing include `vulkan-validation-layers`, `nvidia-prime`, `nvidia-settings`, and `glxgears`. You may wish to install different or additional packages depending on your preferences and use case.

For my testing machines, the package installation looked like this:

```
sudo pacman -S nvidia-dkms vulkan-validation-layers nvidia-prime nvidia-settings glxgears
```
{ }

## Enable Early Module Loading

You will need to add the `nvidia_drm.modeset=1` and `nvidia_drm.fbdev=1` kernel parameters to enable early module loading. Open your Grub config file with the editor:
```
micro /etc/default/grub
```
Add the kernel parameters between the quotes on the `GRUB_CMDLINE_LINUX_DEFAULT=` line. For example, it may look something like this depending on the other kernel parameters you have set there:
```
...
GRUB_CMDLINE_LINUX_DEFAULT='quiet loglevel=3 nvidia_drm.modeset=1 nvidia_drm.fbdev=1'
...
```
Save and exit, then regenerate the Grub configuration file.
```
sudo update-grub
```
Add the modules to the initramfs by setting up a dracut config.
```
micro /etc/dracut.conf.d/nvidia.conf
```
```
force_drivers+=" nvidia nvidia_modeset nvidia_uvm nvidia_drm "
```
Save and exit, then rebuild the initramfs.
```
sudo dracut-rebuild
```

{ }

## Additional Environment Variables

Additional environment variables may need to be set before Sway is launched. What environment variables should or should not be set will depend on your specific setup. During the course of my research I found quite a few environment variables that users needed to set for various reasons. I have included several common ones here as a reference.

I found I did not need to set *any* additional environment variables to get a Sway session with Nvidia drivers loaded on the machines I tested. It may be simpler to skip adding additional variables at first, then add them one at a time as you determine they are needed.

Some of the environment variables caused certain things to break on my test machines--even preventing the session from loading altogether in the case of `WLR_RENDERER=vulkan`. I have included these variables here anyway, since some users have reported they were needed for their specific setup.

{ }

#### `WLR_NO_HARDWARE_CURSORS=1`
   - Purpose: This variable is associated with wlroots, a library for building compositors for Wayland. Setting this variable to 1 disables hardware cursors when using wlroots-based compositors.
   - When to set: If you are experiencing issues with hardware cursors or if the compositor you are using recommends or requires this setting.

#### `XWAYLAND_NO_GLAMOR=1`
   - Purpose: This variable is related to XWayland, a compatibility layer that allows X11 applications to run on the Wayland display server. Setting this variable to 1 disables the use of Glamor acceleration for XWayland.
   - When to set: If you are encountering graphical glitches or performance issues with XWayland, you might try setting this variable.
   
> **Note:** During testing, setting the `XWAYLAND_NO_GLAMOR=1` variable caused some X-Wayland apps to fail to launch.{.is-warning}

#### `GBM_BACKEND=nvidia-drm`
   - Purpose: GBM (Graphics Buffer Manager) is a component in the graphics stack. Setting this variable to "nvidia-drm" specifies the GBM backend to be used with the NVIDIA Direct Rendering Manager (DRM).
   - When to set: If you are facing compatibility or performance issues with GBM, setting this variable may help.

#### `__GLX_VENDOR_LIBRARY_NAME=nvidia`
   - Purpose: This variable specifies the OpenGL vendor library to be used. Setting it to "nvidia" explicitly selects the NVIDIA OpenGL vendor library.
   - When to set: If you have multiple OpenGL implementations installed and want to ensure that the NVIDIA OpenGL library is used.

#### `LIBVA_DRIVER_NAME=nvidia`
   - Purpose: This variable is related to VA-API, a programming interface for hardware-accelerated video playback. Setting it to "nvidia" specifies the NVIDIA video acceleration driver to be used.
   - When to set: If you want to ensure that the NVIDIA video acceleration driver is used with VA-API.

#### `WLR_RENDERER=vulkan`
   - Purpose: This variable is related to wlroots, the Wayland compositor library. Setting it to "vulkan" specifies the renderer backend to be Vulkan.
   - When to set: If your graphics stack and drivers support Vulkan, and you want to use Vulkan as the rendering backend for the compositor.

> **Note:** During testing, Sway would not launch when the `WLR_RENDERER=vulkan` variable was set (even with `vulkan-validation-layers` installed). When it is not set Sway will fall back to using GLES2.{.is-warning}


Again, please note these options are listed here as a reference--not as a specific recommendation. These variables are often used as either workarounds for common issues, or optimizations for specific hardware, driver, or software combinations. Whether you should set them or not depends on your specific setup and whether you encounter any issues that these variables might address. If you are not experiencing any problems, leaving them unset is generally fine.

{ }

### How to set the environment variables

When using the ReGreet greeter, these variables can be added to `/etc/greetd/regreet.toml`. The format they are written is like so:
```
VARIABLE_NAME = "value"
```
Each value ***must*** be wrapped in quotes, or ReGreet will fail to read the config and none of the settings in the config will be applied. A giveaway this has happened is the greeter background will be solid grey instead of showing the wallpaper you have set.

To add the environment variables you wish to add to your ReGreet config, open it with your editor:

```
micro /etc/greetd/regreet.toml 
```
In the `[env]` section, add whatever additional environment variables you need below the other environment variables:
```
...
WLR_RENDERER = "vulkan"
WLR_NO_HARDWARE_CURSORS = "1"
XWAYLAND_NO_GLAMOR = "1"
GBM_BACKEND = "nvidia-drm"
__GLX_VENDOR_LIBRARY_NAME = "nvidia"
LIBVA_DRIVER_NAME = "nvidia"
...
```
> These variables are listed as an example only; as mentioned above, the variables you may need to add will depend on your specific setup.{.is-info}

{ }

### Setting environment variables without ReGreet

Users who are not using the ReGreet greeter will need to set these variables another way, for example by using a wrapper script to launch Sway as described in the Greetd wiki: https://man.sr.ht/~kennylevinsen/greetd/#how-to-set-xdg_session_typewayland {.is-info}

{ }

## Add the `--unsupported-gpu` Flag

Sway will not launch with the proprietary Nvidia drivers loaded unless you pass the `--unsupported-gpu` flag. Most likely this is their way of forcing the user to acknowledge that they are intentionally using an unsupported configuration and may run into issues that are beyond the scope of what the project is able to address.

When using the Greetd display manager, you can add this flag in the main config.

```
micro /etc/greetd/config.toml
```
Add the flag right after the call to `sway`:

```
[default_session]
command = "sway --unsupported-gpu --config /etc/greetd/sway.cfg > /dev/null 2>&1"
```

> If you are using a wrapper script to launch the session, replace `sway` with the command to run your script and pass the `--unsupported-gpu` in your wrapper script instead.
>
> If you are using the ReGreet greeter you do not need to set up a wrapper script.{.is-info}

In addition to the Greetd config, you will need to add the `--unsupported-gpu` flag to the desktop file used to launch the session if you are using a greeter that sources the desktop files to populate session choices on the login screen. A best practice is to set up a new desktop file instead of directly modifying `/usr/share/wayland-sessions/sway.desktop`, because that one will be overwritten every time the package is updated.

You can either make a copy of `/usr/share/wayland-sessions/sway.desktop` and modify the copy (rename it and add the flag), or you can just make a new file with your editor:

```
micro /usr/share/wayland-sessions/sway-nvidia.desktop
```

Paste in the contents for the file:

```
[Desktop Entry]
Name=Sway-Nvidia
Comment=Sway with Nvidia
Exec=sway --unsupported-gpu
Type=Application
```
From the login screen, select the new session ("Sway-Nvidia" in this example) from the session drop-down before you sign in.

![sway_nvidia_regreet.png](/screenshots/sway_nvidia_regreet.png)

That's it! You should now be running Sway with the proprietary Nvidia drivers installed.

![sway_nvidia_gears.png](/screenshots/sway_nvidia_gears.png)