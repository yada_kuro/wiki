---
title: GNOME Cheatsheet/FAQ
description: 
published: true
date: 2022-04-15T23:26:27.409Z
tags: cheatsheet, gnome
editor: markdown
dateCreated: 2022-04-14T02:14:10.393Z
---

## Intro to GNOME
So, you just installed Garuda GNOME for the first time. Whether you're entirely new to the GNOME desktop or a returning user, this page is your one-stop-shop for frequently asked questions, tips and tricks, troubleshooting, and guidelines for getting more help.

## FAQ
* Why can't I use Wayland?

For now, logging into GNOME under Wayland is disabled out of the box in Garuda GNOME. Initially, this was to offer a functional experience from first boot for graphics cards that do not work with Wayland, but this may change in the future.

* Well, how do I enable it?

The Garuda Assistant has the option, titled "GDM Wayland," at the very bottom of the "Settings" tab. Alternatively, you can edit `/etc/gdm/custom.conf` and simply change the line `WaylandEnable=false` to `#WaylandEnable=false`. A reboot may be required for this to take effect.

* Help! I enabled Wayland and now all my Garuda apps are broken!

Depending on the version of Garuda Assistant you're using, it may have added some lines to `/etc/environment` that cause problems for Qt-based applications under Wayland. More detailed information on this can be found in the "Troubleshooting" section.

* I upgraded to GNOME 42 and now `x` thing is broken and all my titlebars look weird, why?

GNOME has the tendency to break things that worked under the previous version in major upgrades. GNOME 42 is very new at the time of writing, and this is a normal part of the GNOME experience. For broken features, check the "Troubleshooting" section, and for visual inconsistencies see "Theming on GNOME."

* I want to use GNOME but don't like some of the default behaviors or can't live without `x` feature from some other DE, how can I make it better?

Look through the Settings first, then check if there's a [GNOME Shell Extension](https://extensions.gnome.org/) that will do what you're looking for. These can be installed from your browser as long as `chrome-gnome-shell` is installed and the "GNOME Shell integration" extension is installed on your browser. Most extensions can be further configured via the Extensions application.

> **Remember that GNOME Extensions *can break things*, so if you have issues make sure to disable all your extensions before assuming it is some other problem with GNOME or Garuda.**
{.is-warning}

If there's something you want to configure that isn't available in the Settings or Tweaks applications, something like `dconf-editor` may be able to do so. However, this is a **powerful tool** and may expose settings that it is not advisable to change, so exercise caution.

* How do I view/set keyboard shortcuts?

The GNOME Settings application has everything you're looking for under the "Keyboard" section. Occasionally, some of these get overwritten by major upgrades to GNOME, but are easy to change back.

* Something I want to know // need help with isn't here, what do I do?

Check the "External Resources" section. ***If*** you read these resources, then search for your own information (see [this guide](https://wiki.garudalinux.org/en/finding-solutions)), and still can't find what you need, follow [this guide](https://forum.garudalinux.org/t/how-to-write-a-nicely-formatted-post-in-discourse-forum/4814) to creating a Garuda Forum post, making sure to include the output of `garuda-inxi` with appropriate formatting and a detailed description of your problem and the steps you've taken so far.

## Theming on GNOME
The state of theming on GNOME is fragmented and oftentimes confusing. However, it is possible to achieve a lovely, consistent look with a little work. This guide assumes you are running GNOME 42, but most steps are applicable to GNOME 40+.

> **Remember that themes *can break things*, so if you have issues make sure to disable your themes before assuming it is some other problem with the GNOME or Garuda.**
{.is-warning}

#### General Information
* Themes for the GNOME Shell and GTK2/3/4 apps can be found on www.gnome-look.org but it is important to look at when a theme was last updated. If it does not support GNOME 40+ and/or GTK4, it will likely not work and may cause problems.
* To install themes just for your user, place them in `~/.themes/` and for global installation, place them in `/usr/share/themes/`. Some themes also include an installer script, which may be the preferred method for that theme.
* To apply themes, use the GNOME Tweak tool that is packaged with the Garuda GNOME edition. Make sure that the Tweaks app is up to date with the current GNOME desktop version.
* GNOME *Shell* themes and *GTK* themes are different. The Shell themes apply to your desktop environment - the top panel, the dash, etc. while the GTK themes are for changing the look of your applications. They can be set independently of one another but many themes supply both.

#### GNOME Shell Theming
* At the time of writing, only the [Flat Remix](https://www.opendesktop.org/p/1013030) themes have been updated to fully support the GNOME 42 shell. It's recommended to install these in `~/.themes` and not from the AUR, as the AUR package also installs a GDM theme, which is generally not recommended.
* It is *possible* to use the Tweak tool to apply GNOME 40-41 Shell themes, but even themes up to date for GNOME 41 may not work fully or cause visual oddities in the top panel menus and volume/brightness indicators under GNOME 42.
	* Note: Inspect the `gnome-shell` folder for your chosen theme. It seems that themes are more likely to work if the theme uses an `assets` folder for visual elements than if there is a `.gresource` file instead.

#### GTK2/3/4 Application Theming
With GNOME 42, a number of changes were introduced to GTK4 theming as GNOME moves towards using its new "Human Interface Guidelines" and migrates to "LibAdwaita" for GTK-native applications. Unfortunately, this means that even within the GNOME default apps, some will use the "old" Adwaita titlebars/theme, while a select handful will use the new GNOME 42 // LibAdwaita titlebars/theme. Over time, all the native GNOME applications will be migrated to these new styles, but as it stands there are visual inconsistencies in GNOME's default appearance.

* To achieve a consistent "default" appearance, there is a port of the new LibAdwaita theme for applications that have not been moved over to the new framework called `adw-gtk3` in the AUR which can be applied via the Tweak tool. Remember to set the associated light/dark option in Settings.
* *Most* applications still use the "old" GTK3/4 framework, so the standard method of installing GTK themes per-user or globally, then setting them in the Tweak tool, still applies. This option is now called "Legacy Applications," but works the same way.
* For themes that have been updated to include a LibAdwaita theme (currently only [Flat Remix](https://www.gnome-look.org/p/1214931)), the contents of the theme's "LibAdwaita" folder - `gtk.css` and `assets/` - must be moved into `~/.config/gtk4.0/` in addition to applying the "Legacy" theme from the Tweak tool.
* If a theme only includes the "old" GTK4 theme, it is possible to force the new LibAdwaita apps to use it, but this method is not recommended and may cause breakage or visual oddities. If you still wish to do so:
	* Apply the theme via the "Legacy Applications" option in the Tweak tool.
	* Ensure that `~/.config/gtk4.0` is empty or only contains `settings.ini`.
	* If you are on Wayland, edit `/etc/environment` and add the line `GTK_THEME=` followed by the name of your theme. This cannot include any spaces, and capitalization must be correct. It is usually the same as the name that appears in the "Legacy Applications" option, for example `GTK_THEME=Adwaita-dark` This must be done as root, such as by `sudo micro /etc/environment`.
	* If you only use GNOME on Xorg, it is better to edit `~/.profile` and add the above line, but this will only apply to Xorg sessions. This can be edited without root access.
	* Reboot.

#### Non-GTK Application Theming
* For Qt-based applications (like the Garuda tools) it may not be possible to achieve perfect consistency, but under GNOME 42 most Qt apps will use at least the native titlebars.
	* If you have issues with this, check the output of `printenv`. If any `QT_QPA_` variables like `PLATFORM` or `PLATFORMTHEME` are set, this may be the source of the problem. Check `~/.profile` and `/etc/environment` append a `#` to the beginning of any relevant lines to troubleshoot this.
* For Flatpak applications, some themes include a Flatpak version. The installation instructions for this can be found per-theme. For more information on theming Flatpaks, see [this Gist](https://gist.github.com/lmintmate/b512680c0d1ee8f41b8f43cd2c81dc2c).
* For Electron-based applications, some (like `code-oss`) will use the native titlebars and include themes for the app itself. Others will stubbornly refuse to cooperate with your theme, but [this section](https://wiki.archlinux.org/title/Wayland#GUI_libraries) of the Wayland ArchWiki page may be of help.
	* The best way forward if visual consistency is very important to you is to find non-electron alternatives.
* More specific theming configurations, like those for shells such as `bash`, various terminals like `kitty`, etc, must be done on a per-application basis. A color-picker app like `gcolor3` can be very useful if you want to port an existing theme.

#### GDM Theming (!!!DANGER!!!)
> **It is *not recommended* to theme/modify the GNOME Display Manager unless you know what you're doing and are capable of reverting/reinstalling GDM from a command line if the display manager breaks.**
{.is-danger}

* If you plan to make any changes to GDM's configurations, make a backup of all files you plan to change, and be sure to document the changes you make should things go wrong.
* Some themes are available for GDM on www.gnome-look.org but unless they have been updated for the latest version of GDM applying them is *highly inadvisable*.
* It is possible to manually configure options for the GDM. The process is outside of the scope of this page, and you do so at your own risk.
* At the time of writing, a GUI configuration package called `gdm-settings-git` exists in the AUR that is up to date for the most current GNOME and GDM version. However, it is in early development and not associated with the GNOME project. Again, proceed with caution.

## Troubleshooting
#### General Information
As with troubleshooting anything on Arch-based distributions, it is important to try to figure out the issue on your own first. To make this easier for yourself and everyone else:
* Before troubleshooting ANY problem under GNOME, revert entirely to the default theme (including any changes made to files in accordance with the above theming guide) *AND* disable extensions entirely via the Extensions app. If one or both of these fixes your problem, it should be reported to the relevant extension or theme in accordance with their guidelines.
* If you have enabled Wayland, check if the problem is present under *both* GNOME and GNOME on Xorg. Wayland is known to occasionally cause problems with certain graphics cards and some applications.
* GNOME comes with a nice, searchable application called Logs for viewing system logs, which is a very useful tool, but don't shy away from the command line if other logs may be relevant.
* The [Arch Wiki](https://wiki.archlinux.org/) is a fantastic resource and is easy to search. If you've narrowed the issue at all, searching for associated keywords there is, if nothing else, a good place to start.
* Make sure to search the [Garuda Forum](https://forum.garudalinux.org/) as well to see if anyone has had the same issue.

#### Common Problems
* Qt-based applications (like the Garuda tools) fail to launch.
	* Check if this happens exclusively under Wayland or Xorg.
	* Check `/etc/environment` and if using Xorg  `~/.profile`. If any of: `QT_QPA_PLATFORM`, `GT_QPA_PLATFORMTHEME`, and `XDG_SESSION_TYPE` are present, try adding a `#` before the variable name (i.e. `#XDG_SESSION_TYPE=wayland`) one at a time, re-booting after each change.
* GNOME's Online Accounts feature appears broken after updating to GNOME 42.
	* Log in under Wayland (enabling it as explained in the "FAQ" if you have not done so). At the time of writing, Online Accounts are not configurable under Xorg but its overall functionality is not broken.
* GDM fails to start or is missing core functionalities.
	* Disable Wayland in `/etc/gdm/custom.conf` with the line `WaylandEnable=false`. This will prevent GDM from trying to start with the Wayland backend, but also disable logging into the GNOME Shell under Wayland.
	* If you're on NVIDIA hardware and/or the above option does not work, see [Troubleshooting GDM](https://wiki.archlinux.org/title/GDM#Troubleshooting).
	* If you've made any changes to GDM's configuration, revert them and/or fully reinstall GDM.
* FireFox or related browsers have graphical glitches under Xorg.
	* Make sure `MOZ_ENABLE_WAYLAND=1` is not present in `/etc/environment` or `~/.profile`.
	* If you want to use the Wayland backend under Wayland but not under Xorg, put `MOZ_ENABLE_WAYLAND=1` in `/etc/environment`, *and* put `MOZ_ENABLE_WAYLAND=0` in `~/.profile`.
* Auto-start applications (such as those in `~/.config/autostart/`) do not launch at login.
	* It is still unclear why this happens sometimes under GNOME, but there are a few steps to try.
	* For new entries, try using the Tweak tool to add autostart appplications instead of doing so manually by file.
	* For the built-in `.desktop` files from Garuda that are meant to be run only the *first* time you log in (like `bashrc-setup.desktop`), just open the file with a text editor and manually run the command following `exec=`. (in this case, `mv ~/.bashrc_garuda ~/.bashrc`) and then remove the `.desktop` from `~/.config/autostart/`.
		* Note that the commands in `initial-user-setup.desktop` do not appear to be relevant to things packaged with the GNOME edition.
	* For your own applications, if the Tweak tool does not work, make a fresh copy of the system `.desktop` file, usually found in `/usr/share/applications/` to your `~/.config/autostart/`, then check that it appears and is enabled under the "Startup Applications" category in the Tweak tool.
	* For the autostart files supplied by Garuda for applications not packaged with the GNOME edition, it is safe to delete these files. If you install one of the listed applications (for example Variety) later on, it may be necessary to make a new copy as mentioned in the previous step. If you instead want the Garuda-specific configs, copy the `.desktop` out of `/etc/skel/.config/autostart/`.

## Final Notes
This page is not comprehensive, and the external documentation below is the best place to learn more about anything covered here. This page is currently up-to-date for the initial release of GNOME 42 and the 03/29 `.iso` of the Garuda GNOME edition.

## External Resources
* [GNOME ArchWiki Page](https://wiki.archlinux.org/title/GNOME)
* [GDM ArchWiki Page](https://wiki.archlinux.org/title/GDM)
* [Wayland ArchWiki Page](https://wiki.archlinux.org/title/Wayland)
* [NVIDIA ArchWiki Page](https://wiki.archlinux.org/title/NVIDIA)
* [GNOME 42 Release Notes](https://release.gnome.org/42/)
* [Themes on GNOME-LOOK](https://www.gnome-look.org)
* [Environment Variables ArchWiki Page](https://wiki.archlinux.org/title/Environment_variables)
	* See also: [here](https://github.com/swaywm/sway/wiki/Running-programs-natively-under-wayland), [here](https://discourse.ubuntu.com/t/environment-variables-for-wayland-hackers/12750), and [here](https://discourse.ubuntu.com/t/gtk-backend-selection-or-why-gtk-cannot-open-display-0/17657)