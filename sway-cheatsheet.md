---
title: Sway Cheatsheet
description: 
published: true
date: 2024-01-20T18:45:29.147Z
tags: 
editor: markdown
dateCreated: 2024-01-20T18:44:50.954Z
---


> **Note:** the default <kbd>Mod</kbd> key is the Meta/Super key.
> * On a Windows keyboard, this will be the "Windows" key.
> * On an Apple keyboard, this will be the "Command" key.
>
> If you wish, you can change the <kbd>Mod</kbd> key to something else in your Sway config.
{.is-info}




## General Keybindings
| Description                    | Keybinding                    |
| ------------------------------ | ------------------------------|
| Change window focus            | <kbd>Mod</kbd> + arrow keys  |
| Vim keys are supported         | <kbd>Mod</kbd> + <kbd>H</kbd> <kbd>J</kbd> <kbd>K</kbd> <kbd>L</kbd> |
| Close a window                 | <kbd>Mod</kbd> + <kbd>Backspace</kbd> |
| Move a window                  | <kbd>Mod</kbd> + <kbd>Shift</kbd> + arrow keys |
| Move window with mouse         | <kbd>Mod</kbd> + left click |
| Resize a window                | <kbd>Mod</kbd> + <kbd>R</kbd>, resize with arrow keys, <kbd>Esc</kbd> |
| Resize with right click        | <kbd>Mod</kbd> + right click |
| Change workspace               | <kbd>Mod</kbd> + <kbd>1</kbd>, <kbd>2</kbd>, <kbd>3</kbd>, ... |
| Send window to workspace       | <kbd>Mod</kbd> + <kbd>Shift</kbd> + <kbd>1</kbd>, <kbd>2</kbd>, <kbd>3</kbd>, ... |
| Toggle floating/tiling         | <kbd>Mod</kbd> + <kbd>Space</kbd> |
| Toggle fullscreen              | <kbd>Mod</kbd> + <kbd>F</kbd> |

## Application Shortcuts
| Description                    | Keybinding                    |
| ------------------------------ | ------------------------------|
| Launch the terminal            | <kbd>Mod</kbd> + <kbd>Enter</kbd>|
| Launch the file explorer       | <kbd>Mod</kbd> + <kbd>E</kbd> |
| Launch the web browser         | <kbd>Mod</kbd> + <kbd>B</kbd> |
| Launch the text editor         | <kbd>Mod</kbd> + <kbd>T</kbd> |
| Launch the calculator          | <kbd>Mod</kbd> + <kbd>C</kbd> |

## Launchers and Menus
| Description                    | Keybinding                    |
| ------------------------------ | ------------------------------|
| Open the launcher              | <kbd>Mod</kbd> + <kbd>D</kbd> |
| Open the application menu      | <kbd>Mod</kbd> + <kbd>Shift</kbd> + <kbd>D</kbd> |
| Open the power menu            | <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>Delete</kbd> |
| Switch back to previous window | <kbd>Alt</kbd> + <kbd>Tab</kbd> |
| Activities overview            | <kbd>Mod</kbd> + <kbd>Tab</kbd> |
| Workspace overview             | <kbd>Mod</kbd> + <kbd>Shift</kbd> + <kbd>Tab</kbd> |

## Scratchpad
| Description                    | Keybinding                    |
| ------------------------------ | ------------------------------|
| Move window to scratchpad      | <kbd>Mod</kbd> + <kbd>Shift</kbd> + <kbd>-</kbd> |
| Show next scratchpad window    | <kbd>Mod</kbd> + <kbd>-</kbd> |

## Screenshots
| Description                    | Keybinding                    |
| ------------------------------ | ------------------------------|
| Snip a screenshot              | <kbd>Print</kbd>              |
| Screenshot a window            | <kbd>Ctrl</kbd> + <kbd>Print</kbd> |
| Screenshot the display         | <kbd>Shift</kbd> + <kbd>Print</kbd> |

## Clipboard Manager
| Description                    | Keybinding                    |
| ------------------------------ | ------------------------------|
| Launch cliphist                | <kbd>Ctrl</kbd> + <kbd>Mod</kbd> + <kbd>V</kbd> |
| Delete an entry from cliphist  | <kbd>Ctrl</kbd> + <kbd>Mod</kbd> + <kbd>X</kbd> |

## See more bindings
| Description                    | Keybinding                    |
| ------------------------------ | ------------------------------|
| Shortcut to Sway config        | <kbd>Mod</kbd> + <kbd>Shift</kbd> + <kbd>I</kbd>|
