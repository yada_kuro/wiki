---
title: Dracut
description: Easy-mode initramfs image generation
published: true
date: 2023-02-26T18:51:17.950Z
tags: 
editor: markdown
dateCreated: 2023-01-01T02:33:05.913Z
---

## What is Dracut?
Dracut is a tool for generating initramfs images. Initial development started in 2008 and is credited to the Fedora project.

At the time, Red Hat had a tradition of naming projects after cities and towns nearby to their headquarters in Westford, Massachusetts. In the spirit of that tradition, dracut (the tool) is named after the town of Dracut, Massachusetts. It is pronounced DRAY-cut (dray as in Dr. Dray, or rhymes with clay).

Prior to the Raptor release, Garuda has used mkinitcpio as the default for initramfs image generation.

## Why are we switching to it?
The primary benefit of dracut is it automatically figures out what needs to be in the initramfs image for you--you don't need to do anything.

That means no more hacking around in `/etc/mkinitcpio.conf` trying to figure out if your hooks are in the right order, or if you have the right modules added, or whatever. Dracut takes a look at your system, figures out what you will need to successfully boot, and builds it into the image for you. Nice!

If you *do* need to make any adjustments due to an unusual configuration, testing, or something just not working right, dracut handles its configuration with modules.

## Adding or removing modules
Needing to add or remove modules is uncommon--usually the modules you need will be automatically configured. If you do need to make a change, you can add a config file to `/etc/dracut.conf.d/` and indicate the modules you wish to add or remove.

To add a module (in our example we are adding the `network-legacy` module), put a line into your configuration file like so:
~~~
add_dracutmodules+=" network-legacy "
~~~
Note there are leading and trailing spaces around the module name.

To remove a module (here we are removing the `bluetooth` module):
~~~
omit_dracutmodules+=" bluetooth "
~~~

Modules can also be added or removed in a command line argument, which could be useful if you wish to generate a one-off image that has a special configuration without making adjustments to your existing setup. Command line arguments override configurations specified in any config files.

## Triggering a rebuild
Dracut will automatically rebuild the initramfs if you install or upgrade a kernel or DKMS package, however if you wish to trigger a rebuild for some other reason (adding or removing modules in a dracut config, for example), you can simply run:
~~~
sudo dracut-rebuild
~~~

## Switching from mkinitcpio
Starting with the Raptor release, Garuda ISOs will include dracut by default. Prior releases will not be automatically switched to dracut--these systems will continue to use mkinitcpio unless they are explicitly switched over.

To switch from mkinitcpio to dracut, install the `garuda-dracut-support` package:

~~~
sudo pacman -S garuda-dracut-support
~~~

There will be a few prompts asking to remove mkinitcpio packages; go ahead and confirm them for removal.

That's it! Next time you upgrade or install a kernel or DKMS package, dracut will automatically rebuild the initramfs image for you.

## Learn more
For additional information about the modules dracut uses or advanced configuration options, these are some recommended resources:

* Dracut Wiki: https://github.com/dracutdevs/dracut/wiki
* ArchWiki: https://wiki.archlinux.org/title/Dracut
* Gentoo Wiki: https://wiki.gentoo.org/wiki/Dracut